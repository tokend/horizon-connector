package responses

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTransactionSuccess_Unmarshal(t *testing.T) {
	body := `{
	  "hash": "4669944950f36d0959372149135ea618d5f2818f8b308bb17d0ef39ae0745e2c",
	  "ledger": 3396,
	  "envelope_xdr": "AAAAAM6teoUJhe3QMBVgs6R+iCF1gl+Kkgj0uYAswVAlPFqiTWWCIQf8/VIAAAAAAAAAAAAAAABaXQDTAAAAAAAAAAEAAAAAAAAAAAAAAAAa+RTTsb0Gs/gZp+rujvya7xLmTQYvNwOrmaBtho+wmwAAAAB3crUe1+GbkW22lg/UGCxQIv4vODH4WdX7nkBHaRxIDAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAASU8WqIAAABA/D8Ral8rbiK6IFpW5BK7qa2Eg3pu6VyYyl4/6+FNze4aJ/57MNzRSG1KpAHw6i4IzMLlhpwmjK5O6xYM/o2cDg==",
	  "result_xdr": "AAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAIAAAAAGvkU07G9BrP4Gafq7o78mu8S5k0GLzcDq5mgbYaPsJsAAAABAAAAIjFKYm5oMW1nUnJWZ2czV0NINVk0bVR0V0M4SHdGY2RySzEAAAAAAAAAAAAAGvkU07G9BrP4Gafq7o78mu8S5k0GLzcDq5mgbYaPsJsAAAACAAAAKjB4RjA0NDY4Q0M5YzQyMzU4QTE4M0I5MDJDMjdBNGJjODQwMmJGMTJhQwAAAAAAAAAAAAAAAAAA",
	  "result_meta_xdr": "AAAAAAAAAAEAAAAEAAAAAAAADUQAAAAAAAAAABr5FNOxvQaz+Bmn6u6O/JrvEuZNBi83A6uZoG2Gj7CbAAAAAHdytR7X4ZuRbbaWD9QYLFAi/i84MfhZ1fueQEdpHEgMAQAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUQAAAAJAAAAABr5FNOxvQaz+Bmn6u6O/JrvEuZNBi83A6uZoG2Gj7CbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUQAAAAQAAAAABr5FNOxvQaz+Bmn6u6O/JrvEuZNBi83A6uZoG2Gj7CbAAAAAQAAACIxSmJuaDFtZ1JyVmdnM1dDSDVZNG1UdFdDOEh3RmNkcksxAAAAAAAAAAAAAAAAAAAAAA1EAAAAEAAAAAAa+RTTsb0Gs/gZp+rujvya7xLmTQYvNwOrmaBtho+wmwAAAAIAAAAqMHhGMDQ0NjhDQzljNDIzNThBMTgzQjkwMkMyN0E0YmM4NDAyYkYxMmFDAAAAAAAAAAAAAA=="
	}`
	expected := TransactionSuccess{
		Hash:     "4669944950f36d0959372149135ea618d5f2818f8b308bb17d0ef39ae0745e2c",
		Ledger:   3396,
		Envelope: "AAAAAM6teoUJhe3QMBVgs6R+iCF1gl+Kkgj0uYAswVAlPFqiTWWCIQf8/VIAAAAAAAAAAAAAAABaXQDTAAAAAAAAAAEAAAAAAAAAAAAAAAAa+RTTsb0Gs/gZp+rujvya7xLmTQYvNwOrmaBtho+wmwAAAAB3crUe1+GbkW22lg/UGCxQIv4vODH4WdX7nkBHaRxIDAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAASU8WqIAAABA/D8Ral8rbiK6IFpW5BK7qa2Eg3pu6VyYyl4/6+FNze4aJ/57MNzRSG1KpAHw6i4IzMLlhpwmjK5O6xYM/o2cDg==",
		Result:   "AAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAIAAAAAGvkU07G9BrP4Gafq7o78mu8S5k0GLzcDq5mgbYaPsJsAAAABAAAAIjFKYm5oMW1nUnJWZ2czV0NINVk0bVR0V0M4SHdGY2RySzEAAAAAAAAAAAAAGvkU07G9BrP4Gafq7o78mu8S5k0GLzcDq5mgbYaPsJsAAAACAAAAKjB4RjA0NDY4Q0M5YzQyMzU4QTE4M0I5MDJDMjdBNGJjODQwMmJGMTJhQwAAAAAAAAAAAAAAAAAA",
		Meta:     "AAAAAAAAAAEAAAAEAAAAAAAADUQAAAAAAAAAABr5FNOxvQaz+Bmn6u6O/JrvEuZNBi83A6uZoG2Gj7CbAAAAAHdytR7X4ZuRbbaWD9QYLFAi/i84MfhZ1fueQEdpHEgMAQAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUQAAAAJAAAAABr5FNOxvQaz+Bmn6u6O/JrvEuZNBi83A6uZoG2Gj7CbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUQAAAAQAAAAABr5FNOxvQaz+Bmn6u6O/JrvEuZNBi83A6uZoG2Gj7CbAAAAAQAAACIxSmJuaDFtZ1JyVmdnM1dDSDVZNG1UdFdDOEh3RmNkcksxAAAAAAAAAAAAAAAAAAAAAA1EAAAAEAAAAAAa+RTTsb0Gs/gZp+rujvya7xLmTQYvNwOrmaBtho+wmwAAAAIAAAAqMHhGMDQ0NjhDQzljNDIzNThBMTgzQjkwMkMyN0E0YmM4NDAyYkYxMmFDAAAAAAAAAAAAAA==",
	}
	var got TransactionSuccess
	assert.NoError(t, json.Unmarshal([]byte(body), &got))
	assert.Equal(t, expected, got)
}
