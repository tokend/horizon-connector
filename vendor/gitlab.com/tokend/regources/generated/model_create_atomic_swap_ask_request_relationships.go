/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package regources

type CreateAtomicSwapAskRequestRelationships struct {
	Bid        *Relation `json:"bid,omitempty"`
	QuoteAsset *Relation `json:"quote_asset,omitempty"`
}
